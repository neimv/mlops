
from datetime import datetime

import pandas as pd

import batch


def dt(hour, minute, second=0):
    return datetime(2021, 1, 1, hour, minute, second)


def test_process_data():
    categorical = ['PUlocationID', 'DOlocationID']
    data = [
        (None, None, dt(1, 2), dt(1, 10)),
        (1, 1, dt(1, 2), dt(1, 10)),
        (1, 1, dt(1, 2, 0), dt(1, 2, 50)),
        (1, 1, dt(1, 2, 0), dt(2, 2, 1)),
    ]
    data_compare = [
        ('-1', '-1', dt(1, 2), dt(1, 10)),
        ('1', '1', dt(1, 2), dt(1, 10)),
    ]

    columns = [
        'PUlocationID', 'DOlocationID', 'pickup_datetime', 'dropOff_datetime'
    ]
    df_expected = pd.DataFrame(data_compare, columns=columns)
    df_expected['duration'] = df_expected.dropOff_datetime - df_expected.pickup_datetime
    df_expected['duration'] = df_expected.duration.dt.total_seconds() / 60
    df = pd.DataFrame(data, columns=columns)
    
    df = batch.process_data(df, categorical)

    assert df.shape[0] == 2
    assert df.shape[1] == 5
    assert df_expected.equals(df)

In this second week the goal was learn the flow of Experiment tracking, this was fundamental because is a lifecycle in the MLOps and good use the tools. Like a good example is the use of MLFlow a tool (in python) to get this track of every model, this tool offers the ML lifecycle with:

- tracking of models
- model registry
- manage of projects

Is this is possible save:
- parameter of model
- metrics
- metadata, especial tags to search the model
- artifacts and models to deploy
- source code
- version of code (in case of errors)
- start and end time
- author

In this week also learn the steps in the lifecycle, that is very important to send from develop of model to production and in case of chages know what changes were made, or if another new DS enters in the team know how to create and reproduce the experiment.

In my opinion the main objetive besides of good culture is the collaboration and the shared knowledge.

The repo for the course is in: https://lnkd.in/gqXi9ghY, with the notes and progress.

/ #66DaysOfMLOps #mlopszoomcamp
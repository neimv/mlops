Week of deploys

In this week the main goal is to know how to create a deploy of models of machine learning, it was learned three types of deployments:

- Web Service, using a http service where the model is loaded and exposed, in this case is online and used to response in the moment, generally is created in one docker where is possible use in multiple sites or clouds.
- Batch, this type of deploy is generally used when use a big dataframe, this is scheduled to use in intervals and process the data of this intervals (hourly, daily, monthly) and save in another site.
- Streaming, in this type of deploy is used the model producers and consumers, this work by events, when is created a new register this execute for example a Lambda, this predicts the data and is possible response in the moment or only save in another service, this is very used when is necessary check the data using multiple filters.

In my opinion this is a very very important topic because is necessary to use the model in some place that was centralized, and not only create models to use only to test a hypothesis.

Good tools to this create this deploys are flask or fastapi to create the endpoint in web server, docker to package the model and use in multiple sites, MLFlow to get the model, this is possible get using Python or R, Lambdas and Kinesis in AWS to create a Streaming deploy, is possible use only lambdas to create an endpoint web.

Finally this week was created one script with loaded the model to predict the time of trips of NYC taxis, this was packaging in one docker, and loading the libraries using Pipenv, this save in S3 and DynamoDB using LocalStack. This was a very excited week.

The repo for the course is in: https://lnkd.in/gqXi9ghY and https://gitlab.com/neimv/mlops/-/tree/main/homework_4, with the notes and progress.

/ #66DaysOfMLOps #mlopszoomcamp
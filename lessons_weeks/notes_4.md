# Deploy of models

## Three types of deploy

Is possible deploy a model in three types:

### Batch

This type of deploy is used when the predict is offline, this run regularly, for example is very used when is not necesary in the moment, or process very much data, this is execute every time (hourly, daily, monthly), The data is get from database or files and execute by the date or datetime intervals, is used to save data in another site oro/and create reports.

### Web Service

Generally is used bu http petitions, use a pattern client-server, the user request a petition and the response is in the moment, for this type of deploy is very used a libraries for example flask o fastapi

### Streaming

This is used when is necesary process multiple times the data, this patterns is used producers and consumers, multiple services and multiple consumers in the same time, in this is worked by events.

## Using of web services

The main tool in this is use the flask to expose a endpoint and use Pipenv to manage the dependencies.

To create dependencies and install:

- this install the dependencies `pipenv install scikit-learn flask --python=3.9`
- to activate the env: `pipenv shell`
- if is necesary install package in mode dev: `pipenv install --dev {package}`

If in case of get model from MLFlow:

- The MLFlow server is necesary use with S3 artifact
- Get id of model-run to get from MLFlow
- In my case:
    - Is necesary set_tracking_uri in MLFlow to IP of server
    - Set the AWS_SECRET_KEY and AWS_ACCESS_KEY in the script

## Batch Deploy

To create a script from notebook python: `jupyter nbconvert --to script {name_notebook}`, this deploy is very used with big dataframes, generally creates the predictions and save in another location.

## Streaming

For this type of deploy is used AWS, with elements as Kinesis and Lambda, is used with events, when is created a new element in Kinesis execute the lambda this predicts and save in another location.

To consider, for this get elements is necesary create a Role with IAM, and set the permissions to use elements, one example is create new Role to lambda and attach this policy: LambdaKinesisExecutionRole.
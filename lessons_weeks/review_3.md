En esta semana se hizo uso de la herramienta Prefect, esta herramienta que es open source y esta creada en python, esta se usa para el manejo de workflows por medio de tareas que se pueden ir agregando una tras otra, la ventaja que se tiene es que se puede ver el flujo completo que se va haciendo, asi como los errores, ya que dispone de un log que se va guardando en el servidor, en este mismo se puede hacer el despliegue del script, el cual puede ser cronometrado para ejecutarse cada cierto tiempo. Este tiene una integracion nativa con dask, asi como puede usar docker y kubernetes.

El repositorio del curso esta en: https://lnkd.in/gqXi9ghY, con notas de los avanves y codigo.

/ #66DaysOfMLOps #mlopszoomcamp
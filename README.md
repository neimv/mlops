# MLOps

## About

This project is for save all knowledge of course [MLOps](https://github.com/DataTalksClub/mlops-zoomcamp).

> Note: this proyect is on gitlab because I'm working in build my local infra with gitlab and another tools

### Module 1

In this first module the goal is know that is MLOps and the flow to create this.

I create a model, using like example the video of lesson 1.3 (training a ride duration prediction model), this model was create in the notebook [duration-prediction_homework.ipynb](duration-prediction_homework.ipynb), using the files **fhv_tripdata_2021-01.parquet** to train a model and predict the duration of one trip, and using **fhv_tripdata_2021-02.parquet** to testing the accuracy of the model.

#### how to run notebook:
1. create the virtualenv (good practice): `virtualenv venv` or `virtualenv venv --python=python3`
2. Activate the virtualenv: `source venv/bin/active`
3. install the libraries: `pip install -r requirements.txt`
4. run jupyter lab: `jupyter-lab`
5. start complete notebook

> [notes](./lessons_weeks/notes_1.md)

> [review](./lessons_weeks/review.md)

### Module 2

In this module the goal is know what is the experiment tracking, the way to send a ML model from dev to stage and production, the main tool in this module is MLFlow a tool to get the way.

In resume, I up MLFlow in my machine to track of the experiment, I use NYC green taxis and create a new model, this model is logged in MLFLow, register paths of file to train, test and validate, some parameters of model and `RMSE` (metric of accuracy) to check what parameters is the best to send to `Stage` or `Production`, in the folder `homework_2` has 4 files that is used to preprocess data to register model, the files are:

1. [preproccess data](./homework_2/preprocess_data.py), in this send files to preprocess data and save in another path, with the necesary data to work.
2. [train model](./homework_2/train.py), this train the model, save the files and log the results in MLFlow using autolog, saving the parameters and results of accuracy.
3. [search the best parameter](./homework_2/hpo.py), this search the best parameters and save the results and parameters in MLFlow this not use the autolog.
4. [register model](./homework_2/register_model.py), this search the best parameters from previuos script, and search the best result to promote the best.

> [notes](./lessons_weeks/notes_2.md)

> [review](./lessons_weeks/review_2.md)

### Module 3

In this module the goal is use **Prefect** an open source workflow management, this is a framework to create task and combine into a workflow, this a excellent tools to monitoring their execution and check errors in one platform. This tools is create in python and the workflows is created in script python, other points is use of native dask integrations, integrations of docker and kubernetes.

To start prefect: **prefect orion start**

Is important that if prefect is up in EC2 instance the security group rules is necesary to open port 22 to access by ssh, http/https and tcp/udp in 4200, to run and access from local machine the command is: **prefect orion start --host 0.0.0.0**.

The most common commands are:

- prefect config view -> to show configurations
- prefect config set PREFECT_API_URL="http://{ip}:{port}/api" -> this configure the server to save metrics, workflow and logs
- prefect storage ls -> show all storages
- prefect storage create -> create storage to save workflow
- prefect deployment create {file.py} -> this work for deploy script in the server
- prefect work-queue preview {queue id} -> this show all work to execute in queue
- prefect agent start {queue id} -> this execute a workflows in the queues

> [review](./lessons_weeks/review_3.md)

### Module 4

In this module the goal is learn about deploy of models, exist 3 types of deploys:

- web service
- batch
- streaming

Also is used the deploy with MLFlow to get the model and deploy with docker, the main tools used in this module is, Flask to expose one endpoint, docker to package the python script and use in multiple scenaries, MLFlow to get the model, Pipenv to control the dependencies of script, another tools used is in AWS, lamdbas to create a simple function and predict data and kinesis to manage the event and call the lambda.

Finally this week was created one script with loaded the model to predict the time of trips of NYC taxis, this was packaging in one docker, and loading the libraries using Pipenv, this save in S3 and DynamoDB using LocalStack. The [code](./homework_4/starter.py) and the [docker](./homework_4/batch.Dockerfile)

> [notes](./lessons_weeks/notes_4.md)

> [review](./lessons_weeks/review_4.md)

### Module 5

### Module 6

### Module 7

### Project


#### Special notes

I create a docker-compose with some services to execute MLFlow, the services are:

- postgresql version 14
- adminer, to connect and view db postgresql
- ftp, a server ftp to save models
- jupyter notebook to create models and analytics
- MLFlow
- airflow
- minio - s3
- nginx

With this compose is possible check and test the MLFlow, and connect with Jupyter, to run use: `docker-compose up`, I use nginx as a proxy server, in this moment is possible to access to adminer using `adminer.localhost` or to mlflow `mlflow.localhost`, so is possible access to jupyter with `jupyter.localhost` but does not work fine, is necesary another configuration (I'm working on it), in all services the user, password or database is `mlflow`, but, is possible to change by other using .env file.

FROM agrigorev/zoomcamp-model:mlops-3.9.7-slim

COPY [ "Pipfile", "Pipfile.lock", "starter.py", "./" ]
RUN pip install pipenv --no-cache-dir
RUN pipenv install --system --deploy


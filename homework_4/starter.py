#!/usr/bin/env python
# coding: utf-8

import os
import pickle

import boto3
import click
import pandas as pd
from pandarallel import pandarallel

FOLDER_OUTPUT = './output/'
pandarallel.initialize(nb_workers=16, progress_bar=True)
AWS_LOCAL = os.getenv('AWS_LOCAL', 'true')


with open('model.bin', 'rb') as f_in:
    dv, lr = pickle.load(f_in)


def read_data(filename, categorical):
    df = pd.read_parquet(filename)
    
    df['duration'] = df.dropOff_datetime - df.pickup_datetime
    df['duration'] = df.duration.dt.total_seconds() / 60

    df = df[(df.duration >= 1) & (df.duration <= 60)].copy()

    df[categorical] = df[categorical].fillna(-1).astype('int').astype('str')
    
    return df


def save_dynamo(row):
    if AWS_LOCAL == 'true':
        dynamo_client = boto3.client(
            'dynamodb',
            endpoint_url="http://localhost:4566"
        )
    else:
        dynamo_client = boto3.client(
            'dynamodb'
        )

    row_dict = row.to_dict()
    item = {
        'IdDate': {'S': row_dict['ride_id']},
        'prediction': {'N': str(row_dict['prediction'])},
        'dispatching_base_num': {'S': str(row_dict['dispatching_base_num'])},
        'pickup_datetime': {'S': str(row_dict['pickup_datetime'])},
        'dropOff_datetime': {'S': str(row_dict['dropOff_datetime'])},
        'PUlocationID': {'S': str(row_dict['PUlocationID'])},
        'DOlocationID': {'S': str(row_dict['DOlocationID'])},
        'SR_Flag': {'S': str(row_dict['SR_Flag'])},
        'Affiliated_base_number': {'S': str(row_dict['Affiliated_base_number'])},
        'duration': {'N': str(row_dict['duration'])},
    }
    dynamo_client.put_item(
        TableName='nyc-taxi-prediction',
        Item=item
    )


@click.command()
@click.option(
    '--year', '-y',
    type=int,
    default=lambda: os.environ.get("YEAR", 0)
)
@click.option(
    '--month', '-m',
    type=int,
    default=lambda: os.environ.get("MONTH", 0)
)
@click.option(
    '--save', '-s', is_flag=True, show_default=True, default=False,
    help="This save the df"
)
def predict(year, month, save):
    if year == 0 or month == 0:
        raise Exception("Error with year and month, is not valid value")

    categorical = ['PUlocationID', 'DOlocationID']
    df = read_data(
        'https://nyc-tlc.s3.amazonaws.com/trip+data/'
            f'fhv_tripdata_{year}-{month:02}.parquet',
        categorical
    )

    dicts = df[categorical].to_dict(orient='records')
    X_val = dv.transform(dicts)
    y_pred = lr.predict(X_val)
    y_pred_mean = y_pred.mean()
    print(f'the mean of predictions is: {y_pred_mean}')

    if save:
        if AWS_LOCAL == 'true':
            s3_client = boto3.client(
                's3',
                endpoint_url="http://localhost:4566"
            )
        else:
            s3_client = boto3.client(
                's3'
            )
        s3_key = f'year={year:04}/month={month:02}/fhv_predictions.parquet'
        save_file = f'{FOLDER_OUTPUT}/fhv_predictions_{year:04}{month:02}.parquet'
        df['ride_id'] = f'{year:04d}/{month:02d}_' + df.index.astype('str')
        df['prediction'] = y_pred
        df.to_parquet(
            save_file,
            engine='pyarrow',
            compression=None,
            index=False
        )

        with open(save_file, 'rb') as data_to_s3:
            s3_client.put_object(
                Body=data_to_s3.read(),
                Bucket='nyc-taxi-predicts-b7aef64',
                Key=s3_key
            )

        print("starting save dynamo")
        print(df.shape)
        df.parallel_apply(save_dynamo, axis=1)
        print("ending save dynamo")


if __name__ == "__main__":
    predict()
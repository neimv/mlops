#!/usr/bin/env python
# coding: utf-8

import pickle

import pandas as pd
from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional


app = FastAPI()
class PredictModel(BaseModel):
    year: int
    month: int
    save: Optional[bool] = False


with open('model.bin', 'rb') as f_in:
    dv, lr = pickle.load(f_in)


def read_data(filename, categorical):
    df = pd.read_parquet(filename)
    
    df['duration'] = df.dropOff_datetime - df.pickup_datetime
    df['duration'] = df.duration.dt.total_seconds() / 60

    df = df[(df.duration >= 1) & (df.duration <= 60)].copy()

    df[categorical] = df[categorical].fillna(-1).astype('int').astype('str')
    
    return df


@app.post('/predict')
def predict(pred: PredictModel):
    categorical = ['PUlocationID', 'DOlocationID']
    year = pred.year
    month = pred.month
    df = read_data(
        f'https://nyc-tlc.s3.amazonaws.com/trip+data/fhv_tripdata_{year}-{month:02}.parquet',
        categorical
    )

    dicts = df[categorical].to_dict(orient='records')
    X_val = dv.transform(dicts)
    y_pred = lr.predict(X_val)
    y_pred_mean = y_pred.mean()

    if pred.save:
        df['ride_id'] = f'{year:04d}/{month:02d}_' + df.index.astype('str')
        df['prediction'] = y_pred

    return {"prediction_mean": y_pred_mean}


if __name__ == "__main__":
    predict()
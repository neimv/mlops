from prefect import flow, task
from typing import List
import httpx

@task(retries=3)
def get_stars(repo: str):
    url = f'https://api.github.com/repos/{repo}'
    count = httpx.get(url).json()["stargazers_count"]
    print(f'{repo} has {count} stars!')


@flow(name='Github stars')
def github_stars(repos: List[str]):
    for repo in repos:
        get_stars(repo)


github_stars(["PrefectHQ/Prefect", "PrefectHQ/miter-design"])


# from prefect.deployments import DeploymentSpec
# from prefect.orion.schemas.schedules import IntervalSchedule
# from prefect.flow_runners import SubprocessFlowRunner
# from datetime import timedelta

# DeploymentSpec(
#     flow=github_stars,
#     name="hello_world",
#     schedule=IntervalSchedule(interval=timedelta(minutes=60)),
#     flow_runner=SubprocessFlowRunner(),
#     tags=["ml"]
# )